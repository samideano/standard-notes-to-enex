import json
import html
from sys import argv
from datetime import datetime
from collections import defaultdict

FLAGS = ['pinned', 'archived', 'trashed']

def flag_to_tag(flag):
    return f'__{flag.upper()}__'

def has_flag(flag, item):
    return item['content'].get(flag, has_deep_flag(flag, item))

def has_deep_flag(flag, item):
    try:
        return item['content']['appData']['org.standardnotes.sn'][flag]
    except KeyError:
        return False

def convert_datetime(extended):
    return datetime.fromisoformat(extended.replace("Z", "+00:00")).strftime("%Y%m%dT%H%M%SZ")

def group_tags_by_uuid(exported):
    tags_by_uuid = defaultdict(lambda: [])

    for key, item in enumerate(exported['items']):
        if item['content_type'] == 'Tag':
            content = item['content']
            tag = content['title']
            uuids = [ref['uuid'] for ref in content['references'] if 'uuid' in ref]

            print(f'{tag} ({len(uuids)})')

            for uuid in uuids:
                tags_by_uuid[uuid].append(tag)
        elif item['content_type'] == 'Note':
            uuid = item['uuid']

            for flag in FLAGS:
                if has_flag(flag, item):
                    tags_by_uuid[uuid].append(flag_to_tag(flag))

    for flag in FLAGS:
        tag = flag_to_tag(flag)
        count = len({k: v for k, v in tags_by_uuid.items() if tag in v})
        print(f'{tag} ({count})')

    return tags_by_uuid

def render_notes(exported):
    tags_by_uuid = group_tags_by_uuid(exported)

    enex = ''

    for key, item in enumerate(exported['items']):
        uuid = item['uuid']
        content = item['content']

        if item['content_type'] == 'Note':
            title = html.escape(content.get('title', 'Untitled'))
            text = html.escape(content['text']).replace('\n', '<br>')

            created_at = convert_datetime(item['created_at'])
            updated_at = convert_datetime(item['updated_at'])

            enex += f'''
            <note>
                <title>{title}</title>
                <content>
                    <![CDATA[
                    <!DOCTYPE en-note SYSTEM "http://xml.evernote.com/pub/enml2.dtd">
                    <en-note>{text}</en-note>
                    ]]>
                </content>
                <created>{created_at}</created>
                <updated>{updated_at}</updated>
            '''

            for tag in tags_by_uuid[uuid]:
                enex += f'''
                <tag>{tag}</tag>
                '''

            enex += f'''
            </note>
            '''

    return enex

def main():
    source = argv[1] if len(argv) > 1 else 'data/notes.txt'

    exported = json.load(open(source))

    enex = f'''
    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE en-export SYSTEM "http://xml.evernote.com/pub/evernote-export3.dtd">
    <en-export export-date="20171228T194211Z" application="Evernote" version="Evernote Mac 6.13.3 (455969)">
    {render_notes(exported)}
    '''

    with open('data/notes.enex', 'w') as f: f.write(enex)

if __name__ == '__main__': main()
